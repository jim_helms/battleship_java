package battleship;
import battleship.Directions.*;

/**
* Represents a ship in the game logic
*/
public class Ship {
    public int Id;                  // Id of the ship
    public int Size;                // Ship size
    public int HitCount;            // Number of hits on this ship - destroyed when Size == HitCount
    public Direction Direction;     // Ship direction
    public Coords Location;         // Ship placement coords

    public Ship(int id, int size) throws Exception {
        Id = id;
        Size = size;
        HitCount = 0;
        Direction = new InvalidDirection();
    }

}


