package battleship;

import java.util.*;

/**
* Represents a grid point on the gameboard
*/
public class GridPoint {
    /**
     * Map CellStatus to StatusMarker (enum to text label for ui)
     */
    private static final Map<CellStatus, String> markerMap = new HashMap<>();
    static
    {
        markerMap.put(CellStatus.Empty, "-");
        markerMap.put(CellStatus.Hit, "X");
        markerMap.put(CellStatus.Miss, "*");
        markerMap.put(CellStatus.Ship, "S");
    }

    private static final Map<CellStatus, String> colorMap = new HashMap<>();
    static
    {
        colorMap.put(CellStatus.Empty, "\u001B[32m");   // GREEN
        colorMap.put(CellStatus.Hit, "\u001B[31m");     // RED
        colorMap.put(CellStatus.Miss, "\u001B[34m");     // BLUE
        colorMap.put(CellStatus.Ship, "\u001B[33m");    // YELLOW
    }

    public CellStatus Status = CellStatus.Empty;
    public int ShipId = -1;

    public String statusMarker() throws Exception {
        return statusMarker(Status);
    }

    /**
     * Returns the viewable version of CellStatus for the UI
     * @param cellStatus
     * @return
     * @throws Exception
     */
    public static String statusMarker(CellStatus cellStatus) throws Exception {
        String statusMarker = statusColor(cellStatus)
                + markerMap.get(cellStatus) + battleship.COLOR_RESET;
        return statusMarker;
    }

    /**
     * Returns to text color of CellStatus for the UI
     * @param cellStatus
     * @return
     * @throws Exception
     */
    public static String statusColor(CellStatus cellStatus) throws Exception {
        String statusColor = colorMap.get(cellStatus);
        return statusColor;
    }
}


