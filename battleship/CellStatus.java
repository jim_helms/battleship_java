package battleship;

/**
 * Indicates the status of a particular GridPoint on the game board
 */
public enum CellStatus {
    Empty,
    Ship,
    Miss,
    Hit
}

