package battleship.InputHandlers;
import battleship.*;

/**
* Handles the exit keyword
*/
public class ExitHandler  extends InputHandler {
    public ExitHandler() throws Exception {
        Type = HandlerType.Action;
    }

    /**
     * Processes the input string and updates the InputResponse game state object
     * @param input
     * @param actionResponse
     * @return
     * @throws Exception
     */
    public boolean process(String input, InputResponse actionResponse) throws Exception {
        boolean error = super.process(input,actionResponse);
        // Perfom base null validation
        if (!error) {
            try {
                if (Input.equals(battleship.ExitKeyword)) {
                    actionResponse.ControlResponse = true;
                    actionResponse.Exit = true;
                }
            } catch (Exception ex) {
                error = true;
                actionResponse.Error = true;
                actionResponse.ErrMessage = String.format(battleship.ErrorFormat, String.format("Error in ExitHandler: %s", ex.getMessage()));
            }
        }
         
        return error;
    }
}


