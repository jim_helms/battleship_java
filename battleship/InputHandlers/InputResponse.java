package battleship.InputHandlers;
import battleship.Coords;

/**
* Represents the state of a user response.  Controls game flow.
*/
public class InputResponse  {
    public boolean Exit = false;
    public boolean Cheat = false;
    public boolean Auto = false;
    public boolean ControlResponse = false;
    public boolean ColValid = false;
    public boolean RowValid = false;
    public Coords Coords = new Coords(0,0);

    public boolean getValid() throws Exception {
        return (ColValid && RowValid);
    }

    public boolean Error = false;
    public String ErrMessage = "";

    public InputResponse(boolean exit, boolean cheat, boolean auto, boolean controlResponse, boolean colValid, boolean rowValid, boolean error, Coords coords, String errMessage) throws Exception {
        Exit = exit;
        Cheat = cheat;
        Auto = auto;
        ControlResponse = controlResponse;
        ColValid = colValid;
        RowValid = rowValid;
        Coords = coords;
        Error = error;
        ErrMessage = errMessage;
    }

    public InputResponse() throws Exception {
        Coords = new Coords();
    }
}


