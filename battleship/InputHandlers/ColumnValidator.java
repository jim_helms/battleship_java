package battleship.InputHandlers;

import battleship.HandlerType;
import battleship.battleship;

/**
* Validates that the input contains a valid column.
*/
public class ColumnValidator  extends InputHandler {
    public ColumnValidator() throws Exception {
        Type = HandlerType.Validation;
    }

    /**
     * Processes the input string and updates the InputResponse game state object
     * @param input
     * @param actionResponse
     * @return
     * @throws Exception
     */
    public boolean process(String input, InputResponse actionResponse) throws Exception {
        boolean error = super.process(input,actionResponse);
        // Perfom base null validation
        if (!error) {
            try {
                // Check valid form and range
                if (Character.isLetter(super.Input.charAt(0))) {
                    int testCol = super.Input.charAt(0) - battleship.UnicodeColumnOffset;
                    // Range check on col
                    if ((testCol < 0) || (testCol >= battleship.GameWidth)) {
                        error = true;
                        actionResponse.ColValid = false;
                        actionResponse.Error = true;
                        actionResponse.ErrMessage = String.format(battleship.ErrorFormat, "Column coordinate out of range!");
                    } else {
                        actionResponse.ColValid = true;
                        actionResponse.Coords.Col = testCol;
                    } 
                } else {
                    error = true;
                    actionResponse.ColValid = false;
                    actionResponse.Error = true;
                    actionResponse.ErrMessage = String.format(battleship.ErrorFormat, "Column coordinate must be a letter!");
                } 
            } catch (Exception ex) {
                error = true;
                actionResponse.ColValid = false;
                actionResponse.Error = true;
                actionResponse.ErrMessage = String.format(battleship.ErrorFormat, String.format("Error in ColumnValidator: %s", ex.getMessage()));
            }
        }
         
        return error;
    }
}


