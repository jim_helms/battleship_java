package battleship.InputHandlers;

import battleship.*;

public class RowValidator  extends InputHandler {
    public RowValidator() throws Exception {
        Type = HandlerType.Validation;
    }

    /**
     * Processes the input string and updates the InputResponse game state object
     * @param input
     * @param actionResponse
     * @return
     * @throws Exception
     */
    public boolean process(String input, InputResponse actionResponse) throws Exception {
        boolean error = super.process(input,actionResponse);
        // Perfom base null validation
        if (!error) {
            try {
                // Check valid form and range
                // Validate row number
                int testRow;
                String testString = Input.substring(1);
                ParsedInteger parsedInteger = new ParsedInteger();
                if (!GameLogic.ValidateInteger(testString, parsedInteger)) {
                    error = true;
                    actionResponse.RowValid = false;
                    actionResponse.Error = true;
                    actionResponse.ErrMessage = String.format(battleship.ErrorFormat, "Row coordinate must be a valid integer!");
                } else {
                    // Range check on row
                    testRow = parsedInteger.Value;
                    testRow--;      // Convert to zero based for internal logic

                    if ((testRow < 0) || (testRow >= battleship.GameHeight)) {
                        error = true;
                        actionResponse.RowValid = false;
                        actionResponse.Error = true;
                        actionResponse.ErrMessage = String.format(battleship.ErrorFormat, "Row out of range!");
                    } else {
                        actionResponse.RowValid = true;
                        actionResponse.Coords.Row = testRow;
                    } 
                } 
            } catch (Exception ex) {
                error = true;
                actionResponse.RowValid = false;
                actionResponse.Error = true;
                actionResponse.ErrMessage = String.format(battleship.ErrorFormat, String.format("Error in RowValidator: %s", ex.getMessage()));
            }
        }
         
        return error;
    }
}


