package battleship.InputHandlers;

import java.util.ArrayList;
import java.util.List;

import battleship.HandlerType;
import battleship.InputHandlers.InputHandler;
import battleship.InputHandlers.InputResponse;
import battleship.battleship;

/**
* The InputHandler base class.  InputHandlers take the input string and modify the inputResponse state object
* depending on each handler's rules.  Each handler calls the base to do any common
* processing - e.g. the example I used was a simple null check and normalizing the
* input to uppercase.
*/
public abstract class InputHandler {
	public String Input;
    public HandlerType Type = HandlerType.Action;
    
    public InputHandler() throws Exception {
        Type = HandlerType.Action;
    }

    /**
    * Gets all input handlers based on valid subclass and skipping the InvalidDirection.
    */
    public static ArrayList<InputHandler> getAll() throws Exception {
    	ArrayList<InputHandler> inputHandlers = new ArrayList<>();
        inputHandlers.add(new ExitHandler());
        inputHandlers.add(new AutoHandler());
        inputHandlers.add(new CheatHandler());
        inputHandlers.add(new ColumnValidator());
        inputHandlers.add(new RowValidator());

        return inputHandlers;
    }

    /**
     * Processes the input string and updates the InputResponse game state object
     * @param input
     * @param actionResponse
     * @return
     * @throws Exception
     */
    public boolean process(String input, InputResponse actionResponse) throws Exception {
        boolean error = false;
        try {
            Input = input.toUpperCase();
            if (input != null && !input.isEmpty()) {
                actionResponse.Error = true;
                actionResponse.ErrMessage = "Error in InputHandler: Input cannot be empty!";
            }
        } catch (Exception ex) {
            error = true;
            actionResponse.Error = true;
            actionResponse.ErrMessage = String.format(battleship.ErrorFormat, String.format("Error in InputHandler: %s", ex.getMessage()));
        }

        return error;
    }
}


