package battleship.InputHandlers;
import battleship.*;

/**
* Handler for auto keyword
*/
public class AutoHandler  extends InputHandler {
    public AutoHandler() throws Exception {
        Type = HandlerType.Action;
    }

    /**
     * Processes the input string and updates the InputResponse game state object
     * @param input
     * @param actionResponse
     * @return
     * @throws Exception
     */
    public boolean process(String input, InputResponse actionResponse) throws Exception {
        boolean error = super.process(input,actionResponse);
        // Perfom base null validation
        if (!error) {
            try {
                if (Input.equals(battleship.AutoKeyword)) {
                    actionResponse.ControlResponse = true;
                    actionResponse.Auto = true;
                }
            } catch (Exception ex) {
                error = true;
                actionResponse.Error = true;
                actionResponse.ErrMessage = String.format(battleship.ErrorFormat, String.format("Error in AutoHandler: %s", ex.getMessage()));
            }
        }
         
        return error;
    }
}


