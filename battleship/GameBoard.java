package battleship;
import java.util.*;
import battleship.*;

/**
* Game UI and board metadata.  Assumes a game board grid with 0,0 in the top left corner.
*/
public class GameBoard {
	
    public int MaxCols;
    public int MaxRows;
    public int PlayerId;
    public GridPoint[][] Grid = new GridPoint[1][1];
    public ArrayList<Ship> Fleet = new ArrayList<>();

    public GameBoard(int Width, int Height, int playerId) throws Exception {
        MaxCols = Width;
        MaxRows = Height;
        PlayerId = playerId;
        Fleet = new ArrayList<Ship>();
        Grid = new GridPoint[MaxCols][MaxRows];
        for (int col = 0; col < MaxCols; col++) {
            for (int row = 0;row < MaxRows;row++) {
                Grid[col][row] = new GridPoint();
            }
        }
    }

    /**
     * Attacks the given target at fireCoords
     * @param fireCoords
     * @return
     * @throws Exception
     */
    public CellStatus attackTarget(Coords fireCoords) throws Exception {
        CellStatus cellStatus = Grid[fireCoords.Col][fireCoords.Row].Status;
        if (!(cellStatus == CellStatus.Hit)) {
            if (Grid[fireCoords.Col][fireCoords.Row].Status == CellStatus.Ship) {
                cellStatus = CellStatus.Hit;
                Fleet.get(Grid[fireCoords.Col][fireCoords.Row].ShipId).HitCount++;
            } else {
                if (Grid[fireCoords.Col][fireCoords.Row].Status == CellStatus.Empty)
                    cellStatus = CellStatus.Miss;
            }
            Grid[fireCoords.Col][fireCoords.Row].Status = cellStatus;
        }
         
        return cellStatus;
    }

    /**
     * Determines if ship is destroyed
     * @param fireCoords
     * @return bool
     * @throws Exception
     */
    public boolean shipDestroyed(Coords fireCoords) throws Exception {
        boolean destroyed = false;
        Ship ship = Fleet.get(Grid[fireCoords.Col][fireCoords.Row].ShipId);
        if (ship.HitCount == ship.Size)
            destroyed = true;
         
        return destroyed;
    }

    /**
     * Determines if all ships are destroyed
     * @return bool
     * @throws Exception
     */
    public boolean fleetDestroyed() throws Exception {
        boolean destroyed = true;
        for (Ship ship : Fleet) {
            if (ship.HitCount < ship.Size) {
                destroyed = false;
                break;
            }
             
        }
        return destroyed;
    }

    /**
     * Write the header
     * @throws Exception
     */
    public void writeHeader() throws Exception {
        System.out.println(battleship.leftMargin + "-- B A T T L E S H I P --");
    }

    /**
     * Write the footer
     * @throws Exception
     */
    public void writeFooter() throws Exception {
        String legend = battleship.leftMargin + "Legend: ";
        for (CellStatus cellStatus : EnumSet.allOf(CellStatus.class)) {
            // Stringbuilder ??
            legend +=  GridPoint.statusMarker(cellStatus) + "=" + cellStatus.toString() + " ";
        }
        String line = battleship.leftMargin +  String.join("", Collections.nCopies(legend.length(), " "));
        System.out.println(line);
        System.out.println(legend);
        System.out.println(line);
    }

    /**
    * Displays this instance.
    */
    public void display(boolean showAllShips) throws Exception {
        System.out.format("%s   Player %d\n", battleship.leftMargin, PlayerId + 1);
        System.out.print(battleship.leftMargin + "   ");

        for (int col = 0;col < MaxCols;col++) {
            System.out.print((char)(battleship.UnicodeColumnOffset + col));
        }

        System.out.println();
        for (int row = 0;row < MaxRows;row++) {
            System.out.format(battleship.leftMargin + "%2d-", row + 1);
            for (int col = 0;col < MaxCols;col++) {
                String statusMarker = Grid[col][row].statusMarker();
                if (!showAllShips) {
                    // Hide ships if not in cheat mode
                    if (Grid[col][row].Status == CellStatus.Ship) {
                        statusMarker = GridPoint.statusMarker(CellStatus.Empty);
                    }
                }
                System.out.print(statusMarker);
            }
            System.out.println();
        }
    }

    /**
     * Gets the name of a given point on the game board
     * @param col
     * @param row
     * @return
     * @throws Exception
     */
    public String getCellName(int col, int row) throws Exception {
        char colLabel = (char)(battleship.UnicodeColumnOffset + col);
        String name = String.format("%c-%d", colLabel, row);
        return name;
    }
}


