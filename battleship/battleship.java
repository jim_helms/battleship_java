package battleship;
import battleship.InputHandlers.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
* Main program entry point
*/
public class battleship {
    private static final int MarginLeft = 2;
    public static final String leftMargin = String.join("", Collections.nCopies(MarginLeft, " "));

    private static final String shipDestroyText = " - Ship destroyed!";
    private static final String promptFormat = leftMargin + "Turn %d - Enter Grid Point (col + row) to attack or enter 'Exit' to leave.\n           ('cheat' Shows all Ships | 'auto' full computer mode)\n";
    private static final String inputFormat = leftMargin + "Turn %d Player %d:";
    private static final String resultFormat = leftMargin + "Last move: Player %d %s %s\n";
    private static final String victoryFormat = leftMargin + "!!!!! Player %d WINS in %d moves !!!!!\n";

    public static final String ErrorFormat = leftMargin + "Error: %s\n";
    public static final int MaxBoardSize = 26;

    public static final int PlayerCount = 2;
    public static final int ShipMinSize = 2;
    public static final int UnicodeColumnOffset = 65;
    public static final String ExitKeyword = "EXIT";
    public static final String CheatKeyword = "CHEAT";
    public static final String AutoKeyword = "AUTO";
    private static final String SizeArg = "SIZE:";
    public static final String COLOR_RESET = "\u001B[0m";
    // Overridable vars
    public static  int GameWidth = 10;
    public static  int GameHeight = 10;
    public static int ShipCount = 4;

    /**
     * Program main entry point
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        gameLoop(args);
    }

    /**
     * This is the main UI control loop which coordinates the GameBoard and GameLogic components
     * @throws Exception
     */
    private static void gameLoop(String[] args) throws Exception {
    	Scanner scanner = new Scanner(System.in);
        try  {   // Just the most basic error handler
            ArrayList<InputHandler> inputHandlers = InputHandler.getAll();
            GameLogic gameLogic = new GameLogic();
            // Process Args - set modes and override default settings
            InputResponse inputResponse = createStartupInputResponse(args);
            ArrayList<GameBoard> gameBoards = gameLogic.setupGameBoards();

            int playerIndex = 0;        // Tracks current player
            int gameTurn = 0;           // Tracks current turn
            String turnResult = "";     // Used to display last turns result

            // Loop until a player uses the exit keyword or until the game is complete, alternating players.
            // Game boards for both players are always shown and there is a cheat mode which makes the ships visible for debugging.
            while (!inputResponse.Exit) {
            	displayGameBoards(gameBoards, inputResponse.Cheat);
                // Display game boards
                System.out.format(promptFormat, gameTurn + 1);
                // Display user prompt
                System.out.println(turnResult);
                // Display last turn's result

                if (!inputResponse.Auto)
                    TimeUnit.SECONDS.sleep(1);

                // Pause to display text to Player before next move
                System.out.format(inputFormat, gameTurn + 1, playerIndex + 1);
                // Display player prompt
                String input;
                if (inputResponse.Auto || playerIndex == 1) {
                    // Auto play check - Will automatically generate the firing coords
                    input = gameLogic.randomizeInputString(false);
                    System.out.print(input);
                }
                else {
                    input = scanner.next();
                }
                // Wait for user input
                // Validate and process input - first reset state flags
                inputResponse.ErrMessage = "";
                inputResponse.Error = false;
                inputResponse.ColValid = false;
                inputResponse.RowValid = false;
                inputResponse.ControlResponse = false;
                inputResponse.Coords = new Coords(0,0);         
                
                for (InputHandler inputHandler : inputHandlers) {
                    // Run each handler to validate the input string
                    boolean processFailed = inputHandler.process(input, inputResponse);
                    // Run handler and check for failure
                    if (processFailed) {
                        System.out.println(inputResponse.ErrMessage);
                        TimeUnit.SECONDS.sleep(2);
                        break;
                    } else if (inputResponse.ControlResponse)
                        break;
                      
                }
                if (inputResponse.getValid()) {
                    // Input validated
                    CellStatus cellStatus = gameBoards.get(playerIndex).attackTarget(inputResponse.Coords);
                    String shipStatus = "";
                    if (cellStatus == CellStatus.Hit) {
                        // Check for ship destruction
                        if (gameBoards.get(playerIndex).shipDestroyed(inputResponse.Coords))
                            shipStatus = shipDestroyText;
                    }

                    boolean gameOver = gameBoards.get(playerIndex).fleetDestroyed();
                    // Check for game win
                    if (gameOver) {
                        // If game is over we can exit
                        displayGameBoards(gameBoards, true);
                        // Show final gameboards
                        System.out.format(victoryFormat, playerIndex + 1, gameTurn + 1);
                        inputResponse.Exit = true;
                    }
                     
                    turnResult = String.format(resultFormat, playerIndex + 1, cellStatus, shipStatus);
                    // Set visual result
                    playerIndex++;
                    if (playerIndex >= PlayerCount) {
                        // Check for turn end
                        playerIndex = 0;
                        gameTurn++;
                    }                     
                }                 
            }
        } catch (Exception ex) {
        	System.out.format(ErrorFormat, "Unexpected error: " + ex.getMessage());
            TimeUnit.SECONDS.sleep(2);
        }
        finally {
        	scanner.close();
        }
    }

    /**
     * Create initial startup response
     * @param args
     * @return InputResponse
     * @throws Exception
     */
    private static InputResponse createStartupInputResponse(String[] args) throws Exception {
        // Arg format battleship.jar [auto] [cheat] [size:nn]
        InputResponse inputResponse = new InputResponse();
        for (String arg : args ) {
            // Check for board size initializer
            if (arg.toUpperCase().startsWith(SizeArg)) {
                ParsedInteger parsedInteger = new ParsedInteger();
                parsedInteger.Value = -1;
                // Validate size
                if (GameLogic.ValidateInteger(arg.substring(arg.indexOf(":") + 1), parsedInteger)) {
                    if ((parsedInteger.Value < 0 ) || (parsedInteger.Value >= MaxBoardSize)) {
                        throw new Exception("Invalid size parameter!");
                    } else {
                        // If it's zero then select random size!
                        if (parsedInteger.Value == 0) {
                            GameHeight = ThreadLocalRandom.current().nextInt(GameHeight, MaxBoardSize);
                            ShipCount = ThreadLocalRandom.current().nextInt(ShipCount, GameHeight / 2);
                        } else {
                            GameHeight = parsedInteger.Value;
                        }
                        GameWidth = GameHeight;
                    }
                }
            }
            // Direct mapping for supported input handlers
            inputResponse.Cheat = (arg.toUpperCase().equals(CheatKeyword) || inputResponse.Cheat) ? true : false;
            inputResponse.Auto = (arg.toUpperCase().equals(AutoKeyword) || inputResponse.Auto) ? true : false;
        }

        return inputResponse;
    }

    /**
     * Displays the gameboards for both players
     * @param gameBoards
     * @param showAllShips
     * @throws Exception
     */
    private static void displayGameBoards(ArrayList<GameBoard> gameBoards, boolean showAllShips) throws Exception {
        clearScreen();
        gameBoards.get(0).writeHeader();
        for (GameBoard gameBoard : gameBoards) {
            gameBoard.display(showAllShips);
        }
        gameBoards.get(0).writeFooter();
    }
    
    /**
    * clear screen
    */
    private static void clearScreen() throws IOException, InterruptedException {
		  if( System.getProperty( "os.name" ).startsWith( "Window" ) )
              new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
          else System.out.print("\033[H\033[2J");
    }
}
