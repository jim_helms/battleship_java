package battleship;

/**
* Class to ease passing coordinates
*/
public class Coords {
    // The coordinate's col and row
    public int Col;
    public int Row;  

    public Coords() {
    }
   
    public Coords(int col, int row) throws Exception {
        Col = col;
        Row = row;
    }

}


