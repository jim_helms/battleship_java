package battleship;
import battleship.Directions.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class GameLogic {
    /**
     * Prepares the game boards, adds ships etc.
     * @return
     * @throws Exception
     */
    public ArrayList<GameBoard> setupGameBoards() throws Exception {
    	ArrayList<GameBoard> gameBoards = new ArrayList<>();
    	try {
            if ((battleship.GameHeight > battleship.MaxBoardSize) || (battleship.GameWidth > battleship.MaxBoardSize)) {
                throw new Exception("Error configured game board size exceeds the possible maximum: " + new Integer(battleship.MaxBoardSize).toString());
            }

            for (int playerIndex = 0; playerIndex < battleship.PlayerCount; playerIndex++) {
                // Setup for each player
                gameBoards.add(new GameBoard(battleship.GameWidth, battleship.GameHeight, playerIndex));
                for (int shipIndex = 0; shipIndex < battleship.ShipCount; shipIndex++) {
                    // Create player ships
                    int shipSize = shipIndex + (battleship.ShipMinSize);
                    Ship ship = new Ship(shipIndex, shipSize);
                    placeShip(gameBoards.get(playerIndex), ship);
                    gameBoards.get(playerIndex).Fleet.add(ship);
                }
            }
        } catch (Exception ex) {
    	    throw new Exception(String.format("Error in setupGameBoards: %s\n", ex.getMessage()));
        }
        return gameBoards;
    }

    /**
     * Randomly places a ship on the board in a random direction
     * @param gameBoard
     * @param ship
     * @throws Exception
     */
    private void placeShip(GameBoard gameBoard, Ship ship) throws Exception {
        try {
            boolean placed = false;
            while (!placed) {    // This retries until both the random gridPoint and the random orientation are valid
                // Get a random grid point
                Coords coords = new Coords();
                coords.Col = ThreadLocalRandom.current().nextInt(0, battleship.GameWidth - 1);
                coords.Row = ThreadLocalRandom.current().nextInt(0, battleship.GameHeight - 1);
                // Check gridPoint - status must be empty or ship
                if (gameBoard.Grid[coords.Col][coords.Row].Status == CellStatus.Empty) {
                    Direction randomDirection = randomizeShipDirection(gameBoard, coords, ship);
                    placed = !randomDirection.Label.equals("Invalid");
                    // If still placeable then update the GameBoard
                    if (placed) {
                        Coords shipCoords = coords;
                        ship.Direction = randomDirection;
                        ship.Location = coords;                         // Store ship loc for debugging
                        for (int i = 0;i < ship.Size;i++) {
                            gameBoard.Grid[shipCoords.Col][shipCoords.Row].Status = CellStatus.Ship;
                            gameBoard.Grid[shipCoords.Col][shipCoords.Row].ShipId = ship.Id;
//                            System.out.format("(%d,%d)", shipCoords.Col, shipCoords.Row);
                            shipCoords = ship.Direction.nextCoords(shipCoords);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw new Exception(String.format("Error in placeShip: %s\n", ex.getMessage()));
        }
    }

    /**
     * Selects a random direction from a set of valid directions for a given random game board cell
     * @param gameBoard
     * @param coords
     * @param ship
     * @return
     * @throws Exception
     */
    private Direction randomizeShipDirection(GameBoard gameBoard, Coords coords, Ship ship) throws Exception {
        Direction direction = new InvalidDirection();
        try {
            ArrayList<Direction> validDirections = getValidDirections(gameBoard,coords,ship);
            // If we have valid orientations left for this x,y pos we use it otherwise mark invalid
            int directionCount = validDirections.size();
            if (directionCount > 0) {
                int index = 0;
                if (directionCount > 1)
                    index = ThreadLocalRandom.current().nextInt(0, directionCount - 1);

                direction = validDirections.get(index);
                ship.Direction = direction;
            }
        } catch (Exception ex) {
            throw new Exception(String.format("Error in randomizeShipDirection: %s", ex.getMessage()));
        }
        return direction;
    }

    /**
     * Gets a list of valid directions for a ship of a given size
     * @param gameBoard
     * @param coords
     * @param ship
     * @return
     * @throws Exception
     */
    private ArrayList<Direction> getValidDirections(GameBoard gameBoard, Coords coords, Ship ship) throws Exception {
        // This list will be culled as validation occurs
        ArrayList<Direction> validDirections = new ArrayList<>();
        try {
            ArrayList<Direction> directions = Direction.getAll();
            for (Direction direction : directions) {
                if (direction.isValid(gameBoard, coords, ship.Size)) {
                    validDirections.add(direction);
                }
            }
        } catch (Exception ex) {
            throw new Exception(String.format("Error in randomizeInputString: %s\n", ex.getMessage()));
        }
        return validDirections;
    }

    /**
     * Provides both a valid and invalid (for testing) input string to support a computer opponent
     * @param badFormat
     * @return
     * @throws Exception
     */
    public String randomizeInputString(boolean badFormat) throws Exception {
        String input;
        try {
            if (badFormat) {
                // Generate a known bad coord letter and a random number then select them randomly to make up the input
                String badNumber = Integer.toString(ThreadLocalRandom.current().nextInt(-999, 999));
                String badLetter = Character.toString((char)(ThreadLocalRandom.current().nextInt(26, 50) + battleship.UnicodeColumnOffset));
                ArrayList<String> badInputs = new ArrayList<>(Arrays.asList(badLetter, badNumber));
                int index1 =ThreadLocalRandom.current().nextInt(0, 1);
                int index2 = index1 == 0 ? 1 : 0;
                input = badInputs.get(index1) + badInputs.get(index2);
            } else
                input = Character.toString((char)(ThreadLocalRandom.current().nextInt(0, battleship.GameWidth) + battleship.UnicodeColumnOffset));
            input += Integer.toString(ThreadLocalRandom.current().nextInt(1, battleship.GameHeight + 1));
        } catch (Exception ex) {
            throw new Exception(String.format("Error in randomizeInputString: %s\n", ex.getMessage()));
        }

        return input;
    }

    /**
     * Validates a string as an integer
     * @param value
     * @param parsedInteger - will be set to int value if returns true otherwise 0
     * @return
     */
    public static boolean ValidateInteger(String value, ParsedInteger parsedInteger) {
        try {
            parsedInteger.Value = Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}


