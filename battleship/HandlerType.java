package battleship;

public enum HandlerType {
    /**
    * Indicates the type of an input handler
    */
    Action,
    Validation
}

