package battleship.Directions;

/**
* Represents the North direction for gamelogic
*/
public class West  extends Direction {
    public West() throws Exception {
        Label = "West";
        ColDelta = -1;
        RowDelta = 0;
    }
}


