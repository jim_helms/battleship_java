package battleship.Directions;

/**
* Represents the East direction for gamelogic
*/
public class East  extends Direction {
    public East() throws Exception {
        Label = "East";
        ColDelta = 1;
        RowDelta = 0;
    }
}


