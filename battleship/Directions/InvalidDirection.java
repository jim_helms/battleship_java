package battleship.Directions;

/**
* Represents an invalid directions state
*/
public class InvalidDirection  extends Direction {
    public InvalidDirection() throws Exception {
        Label = "Invalid";
    }
}


