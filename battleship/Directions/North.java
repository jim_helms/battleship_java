package battleship.Directions;

/**
* Represents the North direction for gamelogic
*/
public class North  extends Direction {
    public North() throws Exception {
        Label ="North";
        ColDelta = 0;
        RowDelta = -1;
    }
}


