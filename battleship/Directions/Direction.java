package battleship.Directions;
import battleship.*;
import java.util.*;

/**
* Represents the direction from the point of a game board grid point.
*/
public abstract class Direction {
    public String Label;
    public int ColDelta;
    public int RowDelta;

    /**
     * Gets the next set of coords along the direction
     * @param coords
     * @return
     * @throws Exception
     */
    public Coords nextCoords(Coords coords) throws Exception {
        Coords nextCoords = new Coords(coords.Col + ColDelta, coords.Row + RowDelta);
        return nextCoords;
    }

    /**
     * Gets a list of all valid directions for use by game logic
     * @return
     * @throws Exception
     */
    public static ArrayList<Direction> getAll() throws Exception {
        ArrayList<Direction> directions = new ArrayList<>();
        directions.add(new North());
        directions.add(new East());
        directions.add(new South());
        directions.add(new West());
        return directions;
    }

    /**
     * Determines if a given game board cell is @ coords is valid for a given ship based on the direction
     * @param gameBoard
     * @param coords
     * @param shipSize
     * @return
     * @throws Exception
     */
    public boolean isValid(GameBoard gameBoard, Coords coords, int shipSize) throws Exception {
        boolean valid = false;
        Coords lastCoords = coords;
        for (int size = 0;size < shipSize - 1;size++) {
            valid = false;
            Coords testCoords = nextCoords(lastCoords);
            if ((testCoords.Col >= 0) && (testCoords.Col < battleship.GameWidth)) {
                // Column range check
                if ((testCoords.Row >= 0) && (testCoords.Row < battleship.GameHeight)) {
                    // Row range check
                    if (gameBoard.Grid[testCoords.Col][testCoords.Row].Status == CellStatus.Empty) {
                        valid = true;
                    } else {
                        break;
                    }

                }
            }
            lastCoords = testCoords;
        }
        return valid;
    }
}


