package battleship.Directions;

/**
* Represents the South direction for gamelogic
*/
public class South  extends Direction {
    public South() throws Exception {
        Label = "South";
        ColDelta = 0;
        RowDelta = 1;
    }
}


