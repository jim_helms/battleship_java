﻿# Battleship
### java 8 test app
###### All game instructions provided at runtime.
Notes:
 - You can use the 'cheat' keyword at any time to see all of the ships on all of the game boards.
 - Most game parameters are configurable via the public constants in Program.cs.
 - If you build in the default debug mode the file bs.cms will run this as an app
 - As is it .net core it can be configured to run on a large variety of OS's, this is just for demo purposes

For fun set the Program.cs constants GameWidth and GameHeight to 26 (max due to use of lettering for cols) and set ShipCount to a higher number.
Next run the app and type 'cheat' then 'auto' or just 'auto' alone and watch it automate the input for both players!  Added this to make testing easier.

Note that I removed my c# unit tests because I didn't want to delay any further implementing an unfamiliar unit test framework.   This should run on windows, linux or the intellij IDE (which has no console clears so it just scrolls).  I tested it on Win10 and  Ubuntu 16 but not OSX.